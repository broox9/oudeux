# README #

This is a pet project for me and me alone

### What is this repository for? ###

* the "Ou" app for finding where you are.  This is strictly a toy app
* version 2.0.0

### How do I get set up? ###

* download the repo
* make sure nodejs is installed locally (currently using v0.10.29)
* make sure npm is installed locally


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact