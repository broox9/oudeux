(function () {



  // = LOCATION CONTROLLER ==============================
  var Ouapp = angular.module("OuApp", []);

  //Lookup Service
  Ouapp.factory('MyLocale', function ($rootScope, $http) {
    var loc = {};
    var goog_api = 'AIzaSyD1KY0AYxsv3GjDFZRrfTQA3MpFAr4zZtU';
    var geocodeurl = 'http://api.geonames.org/neighbourhoodJSON?username=broox9';
    var backup_geocodeurl = 'http://api.geonames.org/findNearbyPostalCodesJSON?username=broox9';
    //var geocodeurl_n = 'https://maps.googleapis.com/maps/api/geocode/json';
    //var geocodeurl_g = 'http://neighborhoodapi.com/v1/neighborhoods.json';

    //loc.searchType = 'locality'; // 'neighborhood' or 'postal_code', or 'locality'
    loc.localeText = "Where Brookes Started";
    //loc.localData = {};

    loc.geoCodeLookup = function (coords) {
      var url = geocodeurl + '&lat=' + coords.lat + '&lng=' + coords.lng;

      $http.get(url).
        success(function (data, status, headers, config) {
          var info = data.neighbourhood;
          console.log(!!info, info);

          if (!info) {
            loc.geoCodeBackup(coords);
            return false;
          }

          loc.localeText = "";
          loc.localeText += info.name;
          loc.localeText += " - ";
          loc.localeText += info.city;
          loc.localeText += ", ";
          loc.localeText += info.adminCode1;

          $rootScope.$broadcast('locale:update', loc.localeText)

        }).
        error(function (data, status, headers, config) {
          console.log('error', data)
        });
    };

    loc.geoCodeBackup = function (coords) {
      var url = backup_geocodeurl + '&lat=' + coords.lat + '&lng=' + coords.lng;
      $http.get(url).
        success(function (data, status, headers, config) {
          var info = data.postalCodes[0]; //first postal code
          console.log('backup data', data);

          loc.localeText = "";
          loc.localeText += info.placeName;
          loc.localeText += ", ";
          loc.localeText += info.adminName2 + " County ";
          loc.localeText += " - ";
          loc.localeText += info.postalCode;

          $rootScope.$broadcast('locale:update', loc.localeText)
        }).
        error(function (data, status, headers, config) {
          console.log('error', data)
        });
    }

    return loc;
  });


  // Coordinates Service
  Ouapp.factory('Coords', function ($rootScope) {

    var coordsvc = {}
    coordsvc.coords = {lat: 40.216339, lng: -74.741427};

    coordsvc.setCoords = function (c) {
      console.log("setting coords", c) ;
      coordsvc.coords = {
        lat: parseFloat(c.lat),
        lng: parseFloat(c.lng)
      };

      $rootScope.$broadcast('coords:update', coordsvc.coords);
    };

    coordsvc.getCoords = function () {
      return coordsvc.coords;
    };

    return coordsvc;
  });



  // Map Service
  Ouapp.factory('MyMap', function ($rootScope, Coords) {
    var mapsvc = {},
      mapList = {};

    mapsvc.map = {};

    mapsvc.setNewMarker = function (coords) {
      var keystring = coords.lat + "_" + coords.lng;
      if (mapList[keystring]) {
       return false;
      }

      mapsvc.currentMarker = new google.maps.Marker({
        position: coords,
        map: mapsvc.map,
        animation: google.maps.Animation.DROP,
        title: "You Are Here"
      });

      mapsvc.currentMarker.setMap(mapsvc.map);
      mapList[keystring] = mapsvc.currentMarker;
    };


    mapsvc.updateMap = function (newcoords) {
      if (_.isEqual(newcoords, mapsvc.coords)) {
        return false;
      }

      mapsvc.map.setCenter(newcoords);
      mapsvc.setNewMarker(newcoords, mapsvc.map);
    };


    mapsvc.createMap = function () {
      var el = document.querySelector("#map-canvas");
      var opts = {
        center: mapsvc.coords,
        zoom: 17
      };


      mapsvc.map = new google.maps.Map(el, opts);
      $rootScope.$broadcast('map:created', mapsvc.map);
    };

    return mapsvc;
  });













  // = MAP CONTROLLER ==============================
  var mapController = Ouapp.controller("MapController", function ($scope, Coords, MyMap) {
    $scope.mymap = MyMap;

    $scope.mymap.createMap();

    $scope.$on('coords:update', function (e, newcoords) {
      console.log('updated coords', newcoords)
      $scope.mymap.updateMap(newcoords)
    });

  });






  // = LOCATION CONTROLLER ==============================
  var localeController = Ouapp.controller("LocaleController", function ($rootScope, $scope, Coords, MyMap, MyLocale) {
    $scope.localeText = MyLocale.localeText;

    $scope.mymap = MyMap;
    $scope.mylocale = MyLocale;
    $scope.coordsvc = Coords;
    $scope.coords = $scope.coordsvc.getCoords();
    $scope.map = {};


    $scope.getLocation = function () {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition($scope.setPosition, $scope.errorPosition)
      } else {
        $scope.disableApp();
      }
    };


    $scope.setPosition = function (position) {
      var newcoords = {lat: position.coords.latitude, lng: position.coords.longitude};


      $scope.samePlace = function () {
        alert("You Haven't Moved")
      };

      $scope.coordsvc.setCoords(newcoords)
      $scope.mylocale.geoCodeLookup(newcoords)
    };

    $scope.errorPosition = function (error) {
      console.log('position error', error)
    },


    $scope.$on('locale:update', function(e, text) {
      $scope.localeText = text;
    });

    // INITIAL LOCATION CALL
    $scope.getLocation();

  });




  // = OPTIONS CONTROLLER ==============================
  var OptionsController = Ouapp.controller("OptionsController", function ($scope) {
      $scope.options = [
        {name: 'Hood', type: 'neighborhood', selectionClass: 'selected'},
        {name: 'Zip/County', type: 'locality',selectionClass: ''}
      ];

      $scope.selected = 0;

      $scope.setSelected = function (index) {
        _.each($scope.options, function (opt) {
          opt.selectionClass = '';
        });
        $scope.selected = 0;
        $scope.options[index].selectionClass = 'selected';
      }

  });




}) ();
