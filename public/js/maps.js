'use-strict';

(function (controller) {
  return false;
  function initialize() {
    var mapOptions = {
      center: defaultStart,
      zoom: 16
    };
    MapObj = new google.maps.Map(document.querySelector("#map-canvas"), mapOptions);

    //my code
   controller.getLocation();

  }

  //start it all on map load
  //google.maps.event.addDomListener(window, 'load', initialize);

})(MapController);

