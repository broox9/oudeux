var _z = Zepto;
defaultStart = {lat: 40.216339, lng: -74.741427}; // St. Frances
MapObj = {}; //to be filled by google

MapController = {
    lastPositionObj: {},
    currentCoords: {},
    currentMarker: {},
    markerList: [],
    gapi: 'AIzaSyD1KY0AYxsv3GjDFZRrfTQA3MpFAr4zZtU',
    //geocodeurl: 'https://maps.googleapis.com/maps/api/geocode/json',
    geocodeurl: 'http://neighborhoodapi.com/v1/neighborhoods.json',
    searchType: 'locality', // 'neighborhood' or 'postal_code', or 'locality'


    disableApp: function () {

    },

    getLocation: function () {
      if (navigator.geolocation) {
        var positionOpts = {
          enableHighAccuracy: true,
          maximumAge: 15000,
          timeout: 6000
        };

        navigator.geolocation.getCurrentPosition(this.setPosition, this.errorPosition, positionOpts);
      } else {
        this.disableApp();
      }
    },

    setPosition: function (position) {
      var coords = {lat: position.coords.latitude, lng: position.coords.longitude};

      if (_.isEqual(coords, this.currentCoords)) {
        //TODO: give a +/- on distance from last point
        alert("you haven't moved");
        return;
      }

      console.log("the coords", coords);
      this.currentCoords = coords;
      MapObj.setCenter(coords);

      this.setNewMarker(coords, MapObj);
      this.geoCodeLookup(coords)
    },

    errorPosition: function (error) {
      console.log('position error', error)
    },

    setNewMarker: function (coordsObj, mapObj) {
      this.currentMarker = new google.maps.Marker({
        position: coordsObj,
        map: MapObj,
        animation: google.maps.Animation.DROP,
        title: "You Are Here"
      });

      this.markerList.push(mapObj);

      if (mapObj) {
        this.currentMarker.setMap(mapObj)
      }
    },

    geoCodeLookup: function (coords) {
//      var self = this;
//      var placeData = {
//        sensor: true,
//        key: this.gapi,
//        result_type: self.searchType //give option for  "county", and "postal code"
//      };
//      $.extend(placeData, {latlng: coords.lat + "," + coords.lng});
//
//      $.ajax({
//        url: self.geocodeurl,
//        data: placeData,
//        //traditional: true,
//        success: function (data) {
//          console.log('returned',data);
//          self.populateInfo(data.status, data.results)
//        }
//      })
      var placeData = {
        lng: coords.lng,
        lat: coords.lat
      };

      console.log('Geo Url', self.geocodeurl);
      console.log('Place Data', placeData)

      $.ajax({
        url: 'http://neighborhoodapi.com/v1/neighborhoods.json', //self.geocodeurl,
        data: placeData,
        dataType: 'json',
        //traditional: true,
        success: function (data) {
          console.log('returned',data);
          //self.populateInfo(data.status, data.results)
        }
      })
    },

     populateInfo: function (status, results) {
       var infoEl = $('#info-result');
       var infoText = '';

       if (status == 'ZERO_RESULTS') {
         infoText = "No " + this.searchType + " info, try another type";
       } else {
         var compoments = results[0]['address_components'];
         infoText = compoments[0]['long_name'] + ", ";
         infoText += compoments[1]['long_name'] + ", ";

         if (compoments.length == 4) {
           infoText += compoments[2]['short_name'];
         } else {
           infoText += compoments[3]['short_name'];
         }


       }
       console.log("infoText", infoText);
       infoEl.text(infoText);
     }
  };

//things bound in callbacks have a scope of window so...
_.bindAll(MapController, 'setPosition', 'errorPosition');

// moved to maps.js
//MapController.getLocation();


  /* DOM READY */
  $(function () {
    /* = Event bindings */
    var goBtn = $('#go-button');
    var optionBtn = $('.search-option');


    goBtn.on('click', function (e) {
      MapController.getLocation();
    });

    optionBtn.on('click', function (e) {
      var mp_con = MapController;
      var currentEl = $(e.target);
      var selectedEl = $('.search-option.selected');

      if (currentEl.hasClass('selected')) {
        return;
      }
      selectedEl.removeClass('selected');
      currentEl.addClass('selected');

      var searchType = currentEl.data('type');
      mp_con.searchType = searchType;
      mp_con.geoCodeLookup(mp_con.currentCoords);
    });
  });





