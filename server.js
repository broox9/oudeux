//= modules ===========================================
var express = require('express');
var bodyParser = require('body-parser')
var app = express();
var api = express();
var mongoose = require('mongoose');


//= config ===========================================
var db = require('./config/db');

var port = process.env.PORT || 3000;
//var mongoose.connect(db.url)

console.log(__dirname)
app.use(express.static(__dirname + '/public'))
app.use(bodyParser.json())       //for to get POST dataz
//  app.use(express.logger('dev'))      //for teh logzes
//  app.use(express.methodOverride())   //for simulationness of DELETE and PUT

/**
 * CORS support.
 */

api.all('*', function(req, res, next){
  if (!req.get('Origin')) return next();
  // use "*" here to accept any origin
  res.set('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.set('Access-Control-Allow-Methods', 'PUT');
  res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type');
  // res.set('Access-Control-Allow-Max-Age', 3600);
  if ('OPTIONS' == req.method) return res.send(200);
  next();
});


//= routes ===========================================
require('./app/routes')(app)


//= start ===========================================
app.listen(port);
console.log("Crank it up on port ", port);
exports = module.exports = app;        //make it public
